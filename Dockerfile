FROM debian:buster-slim

RUN apt update && apt install -y wget \
                                 curl \
                                 parted \
                                 dosfstools \
                                 binutils \
                                 p7zip-full \
                                 sudo \
                                 xz-utils \
                                 jq \
                                 udev

COPY build-image.sh /generator/build-image
COPY init.preinit /generator/init.preinit
COPY init.resizefs /generator/init.resizefs
COPY orangepipc2-boot.cmd /generator/orangepipc2-boot.cmd
RUN chmod +x /generator/build-image

WORKDIR /generator

VOLUME /generator/config
VOLUME /generator/output

ENTRYPOINT ["/generator/build-image"]
